<?php

namespace SoapIntegration\Model\Type;

/**
 * Входные параметры для создания Мсп процедуры
 * @package SoapIntegration\Model\Type
 */
class CreateProcedureMsp
{
    /**
     * Реестровый номер процедуры
     * @var string
     */
    public $registryNumber = '';

    /**
     * ООС гуид процедуры
     * @var string
     */
    public $purchaseGuid = '';
}