<?php

namespace SoapIntegration\Service\OOSProcedures;

use Core_Template;
use DbTable_Procedures;
use Zend_Db_Expr;
use ZipArchive;
use Model_OosExchange;
use Zend_Db_Adapter_Exception;
use Model_Contragent;

class Mock implements IStrategy
{
    private $registryNumber;
    private $version;
    private $locker;
    private $company;

    public function __construct(string $registryNumber, int $version, Model_Contragent $company, $locker)
    {
        $this->version = $version;
        $this->registryNumber = $registryNumber;
        $this->locker = $locker;
        /** @var Model_Contragent $company */
        $this->company = $company;
    }

    /**
     * @param string $guid
     * @return array
     * @throws Zend_Db_Adapter_Exception
     */
    public function getProcedureFromOOS(string $guid)
    {
        $db = getDbInstance();
        $db->getProfiler()->setEnabled(true);

        if($this->version === 1) {
            $addPath = "createProcedure/$guid";
            // "Обнулим" стягиваемую процедуру, если она уже имеется
            $update = [
                'eis_registry_number' => new Zend_Db_Expr('null'),
                'registry_number' => new Zend_Db_Expr('null'),
                'oos_id' => new Zend_Db_Expr('null'),
                'actual' => new Zend_Db_Expr('false')
            ];

            $where = "oos_id = '$guid'";
            $db->update(DbTable_Procedures::NAME, $update, $where);
        } else {
            $addPath = "updateProcedure/$guid";
        }

        // Создание фейковой xml с base64_encoded архивом
        $path = __DIR__ . '/../../resources/examples/oos/' . $addPath . '.xml';
        $zipFileName = tempnam(sys_get_temp_dir(), 'fakeZipEncodedData_');
        $zip = new ZipArchive();
        $zip->open($zipFileName, ZipArchive::CREATE);

        $xml = file_get_contents($path);
        $inn = $this->company->getInn();
        $kpp = $this->company->getKpp();
        $ogrn = $this->company->getOgrn();
        $xml = preg_replace(
            ["/<inn>\d+<\/inn>/", "/<kpp>\d+<\/kpp>/", "/<ogrn>\d+<\/ogrn>/"],
            ["<inn>$inn</inn>", "<kpp>$kpp</kpp>", "<ogrn>$ogrn</ogrn>"],
            $xml
        );

        $zip->addFromString(basename($path), $xml);
        $zip->close();
        $zipEncodedData = base64_encode(file_get_contents($zipFileName));
        $response = $this->getFakeRestXml($zipEncodedData, $guid);
        return Model_OosExchange::rest_publish_all($response, false, false, $this->locker);
    }

    /**
     * Формирует фейковую xml рест-крона для тестов
     *
     * @param string $zipEncodedData
     * @param string $guid
     * @return mixed|string|string[]|null
     */
    protected function getFakeRestXml(string $zipEncodedData, string $guid)
    {
        $template = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
        <ns2:publishedDataResponse 	
            xsi:schemaLocation="http://zakupki.gov.ru/223/integration/schema/TFF-9.3 
            http://zakupki.gov.ru/223/integration/schema/TFF-9.3/Types.xsd" 
            xmlns="http://zakupki.gov.ru/223fz/types/1" 
            xmlns:ns2="http://zakupki.gov.ru/223fz/webRequest/1" 
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" > 
            <header>
                <guid>{packetGuid}</guid>
                <createDateTime>{createDate}</createDateTime>
            </header>
            <ns2:body>
                <ns2:refguid>{packetGuid}</ns2:refguid>
                <ns2:result>success</ns2:result>
                <ns2:publishedData>
                    <ns2:etpResult>OK</ns2:etpResult>
                    <ns2:items>
                        <ns2:item>
                            <ns2:guid>{guid}</ns2:guid>
                            <ns2:entityName>purchaseNotice</ns2:entityName>
                            <ns2:regNumber>{registryNumber}</ns2:regNumber>
                            <ns2:version>{version}</ns2:version>
                            <ns2:publicationDateTime>2019-12-16T12:24:24</ns2:publicationDateTime>
                            <ns2:status>P</ns2:status>
                            <ns2:itemResult>OK</ns2:itemResult>
                        </ns2:item> 
                    </ns2:items>
                    <ns2:zipEncodedData>{zipEncodedData}</ns2:zipEncodedData>
                </ns2:publishedData>                
            </ns2:body>
        </ns2:publishedDataResponse>';

        return Core_Template::process(
            $template,
            [
                'guid' => $guid,
                'registryNumber' => $this->registryNumber,
                'version' => $this->version,
                'packetGuid' => generateUUID(),
                'createDate' => date('c'),
                'zipEncodedData' => $zipEncodedData
            ]
        );
    }

}