<?php

namespace SoapIntegration\Model\Type\Response;

/**
 * Ответ о добавлении мсп процедуры
 * @package SoapIntegration\Model\Type\Response
 */
class CreateProcedureMspResult
{
    /**
     * Id добавленной процедуры
     * @var int
     */
    public $procedureId = 0;

}