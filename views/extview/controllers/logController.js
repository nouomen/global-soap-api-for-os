Ext.onReady(function () {
  Ext.override(Application.controllers.defaultModule.LogController, {
    soapintegrationAction : function (params, app, panel) {
      panel.add({
        xtype: 'Application.components.fullscreenPanel',
        cmpType: 'Application.components.SoapIntegrationLogGrid',
        title: 'Журнал SOAP-API',
        titleIcon: '/ico/soap-integration/soapui-icon.png'
      });
    }
  });
});