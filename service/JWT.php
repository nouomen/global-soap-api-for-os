<?php

namespace SoapIntegration\Service;

use Firebase\JWT\JWT as Token;
use Model_User;
use Exception;

class JWT
{
    const ERROR_CODE = 401;
    private $key;
    protected $ttl = 0;
    protected $algo = 'HS256';

    public function __construct($key, $ttl = null, $algo = null)
    {
        $this->key = $key;
        if (null !== $ttl) {
            $this->ttl = $ttl;
        }
        if (null !== $algo) {
            $this->algo = $algo;
        }
    }

    /**
     * Получить токен
     *
     * @param int $userId
     * @return string
     * @throws Exception
     */
    public function getAuthorizationToken(int $userId): string
    {
        if (!$this->key) {
            throw new Exception('Encoding key not specified', self::ERROR_CODE);
        }
        $now = time();
        return Token::encode([
            'sub' => $userId,
            'exp' => $now + $this->ttl,
            'iat' => $now,
            'nbf' => $now,
        ], $this->key, $this->algo);
    }

    /**
     * Вытащит user id из токена
     * 
     * @param string $token
     * @return int
     * @throws Exception
     */
    public function getUserId(string $token): int
    {
        try {
            $data = Token::decode($token, $this->key, [$this->algo]);
        } catch (Exception $e) {
            // скрываем внутреннее исключение, т.к. в его трейсе будет ключ шифрования
            throw new Exception('Некорректный авторизационный токен',self::ERROR_CODE);
        }

        if (time() > (int)$data->exp) {
            throw new Exception('Авторизационный токен устарел', self::ERROR_CODE);
        }

        $user_id = $data->sub;

        if (!$user_id) {
            throw new Exception('Авторизационный токен недействителен', self::ERROR_CODE);
        }

        return $user_id;
    }

    public function getTtl(): int
    {
        return $this->ttl;
    }
}