<?php
/**
 * Проверяет возможность вызова методов сервиса API в одном месте
 */
namespace SoapIntegration\Service;

use Exception;
use SoapFault;

class APIDecorator
{
    /** @var array Методы сервиса, которые не требуют наличие токена */
    const UNPROTECTED_METHODS = ['getToken'];
    private $api;
    private $access;
    private $procedureId;
    private $userId;
    private $method;
    private $succeed = true;

    public function __construct(API $api, Access $access)
    {
        $this->api = $api;
        $this->access = $access;
    }

    public function getUserId()
    {
        return $this->userId;
    }

    public function getProcedureId()
    {
        return $this->procedureId;
    }

    public function getMethod()
    {
        return $this->method;
    }

    public function getSucceed()
    {
        return $this->succeed;
    }

    /**
     * @param $method
     * @param $params
     * @return mixed
     * @throws Exception
     */
    public function __call($method, $params)
    {
        try {
            if (method_exists($this->api, $method)) {
                $this->method = $method;
                foreach ($params as $param) {
                    $this->procedureId = $param->procedureId ?? null;
                }

                // некоторые методы не требуют авторицации
                if (!\in_array($method, self::UNPROTECTED_METHODS, true)) {
                    $this->access->hasAccess();
                    $token = $this->access->getToken();
                    $this->userId = $this->access->getUserId($token);
                }
                $result = \call_user_func_array([$this->api, $method], $params);

                if(!empty($result->procedureId)) {
                    $this->procedureId = $result->procedureId;
                }

                if(!empty($result->userId)) {
                    $this->userId = $result->userId;
                }

                return $result;
            }
        } catch (Exception $e) {
            $this->succeed = false;
            throw new SoapFault((string)$e->getCode(), $e->getMessage());
        }
    }
}