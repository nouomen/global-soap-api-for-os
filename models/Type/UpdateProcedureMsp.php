<?php

namespace SoapIntegration\Model\Type;

/**
 * Входные параметры для изменения Мсп процедуры
 * @package SoapIntegration\Model\Type
 */
class UpdateProcedureMsp
{
    /**
     * Реестровый номер процедуры
     * @var string
     */
    public $registryNumber;

    /**
     * ООС гуид процедуры
     * @var string
     */
    public $purchaseGuid = '';

    /**
     * Id добавленной процедуры
     * @var int
     */
    public $id = 0;

    /**
     * Версия процедуры
     * @var int
     */
    public $version = 0;
}