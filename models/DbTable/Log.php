<?php
namespace SoapIntegration\Model\DbTable;

use Zend_Db_Table_Abstract;

class Log extends Zend_Db_Table_Abstract
{
  const NAME = 'soap_api_log';
  /**
   * The default table name
   * @access protected
   */
  protected $_name = self::NAME;
  protected $_primary = 'id';

}