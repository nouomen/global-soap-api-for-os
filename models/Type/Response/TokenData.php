<?php

namespace SoapIntegration\Model\Type\Response;

/**
 * Токен
 * @package SoapIntegration\Model\Type\Response
 */
class TokenData
{
    /**
     * Токен
     * @var string
     */
    public $token = '';

    /**
     * Время истечения токена
     * @var int
     */
    public $expired = 0;
}