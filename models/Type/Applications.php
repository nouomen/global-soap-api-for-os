<?php

namespace SoapIntegration\Model\Type;
/**
 * Входные параметры для получения заявок
 * @package SoapIntegration\Model\Type
 */
class Applications
{
    /**
     * Id процедуры
     * @var int
     */
    public $procedureId;

    /**
     * Реестровый номер процедуры ЭТП
     * @var string
     */
    public $registryNumber;

    /**
     * Реестровый номер процедуры ЕИСа
     * @var string
     */
    public $eisRegistryNumber;

    /**
     * ООС гуид процедуры
     * @var string
     */
    public $purchaseGuid;
}