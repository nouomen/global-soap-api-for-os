<?php

namespace SoapIntegration\Model\Type;

/**
 * Входные параметры для получения файла по заявке
 * Class FileTransfer
 * @package SoapIntegration\Model\Type
 */
class FileTransfer
{
    /**
     * Модуль
     * @var string
     */
    public $module;

    /**
     * Тип файла
     * @var string
     */
    public $type;

    /**
     * Идентификатор файла
     * @var int
     */
    public $id;

    /**
     * Первый байт при порционном скачивании файла
     * @var int
     */
    public $start;

    /**
     * Количество байт при порционном скачивании файла
     * @var int
     */
    public $length;
}