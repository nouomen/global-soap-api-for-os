<?php

namespace SoapIntegration\Model\Type;
/**
 * Входные параметры для получения токена
 * @package SoapIntegration\Model\Type
 */
class Authorization
{
    /**
     * Логин
     * @var string
     */
    public $login = '';

    /**
     * Пароль
     * @var string
     */
    public $password = '';
}