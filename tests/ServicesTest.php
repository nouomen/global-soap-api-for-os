<?php
namespace SoapIntegration\Tests;

ini_set('soapServices.wsdl_cache_enabled', 0);

use BaseControllerTestCase;
use DbTable_Procedures;
use DbTable_Users;
use Model_Accreditation;
use Model_Locker_SingletonInstanceMock;
use Model_Lot;
use Model_Procedure;
use SoapIntegration\Model\Application;
use SoapIntegration\Model\Procedure;
use SoapIntegration\Model\Users;
use Zend\Soap\Wsdl\ComplexTypeStrategy\ArrayOfTypeSequence;
use SoapIntegration\Service\API;
use Zend\Soap\AutoDiscover;
use Zend\Soap\Server;
use Zend\Soap\Wsdl\DocumentationStrategy\ReflectionDocumentation;
use SoapIntegration\Model\Log;
use Exception;
use SoapIntegration\Service\Access;
use SoapIntegration\Service\JWT;
use SoapIntegration\Factory\AuthFactory;
use SoapIntegration\Factory\OOSProceduresFactory;
use SoapIntegration\Service\APIDecorator;
use SoapIntegration\Model\Type\CreateProcedureMsp;
use SoapIntegration\Model\Type\UpdateProcedureMsp;
use Zend_Db_Adapter_Abstract;
use Zend_Db_Expr;
use Zend_Exception;
use Zend_Registry;
use SoapIntegration\Service\OOSProcedures\Mock;
use SoapIntegration\Model\Type\FileTransfer;

/**
 * Базовый класс для тестов модуля "Независимый регистратор", запускаемых в контексте площадки.
 */
class ServicesTest extends SoapBaseTest
{
    const PROCEDURE_GUID = '2770ff17-052e-5b42-ecd4-9a9db6ad4d00';
    const PROCEDURE_REGISTRY_NUMBER = '31900097037';
    private $post;
    private $wsdl;
    private $access;
    /** @var JWT */
    private $jwt;

    public function setUp()
    {
        parent::setUp();
        $this->post = getConfigValue('general->site_url') . '/soap-integration/index/post';
        $this->wsdl = getConfigValue('general->site_url') . '/soap-integration/index/wsdl';
        $authFactory = new AuthFactory();
        $this->jwt = $authFactory();
        $this->access = new Access($this->jwt, new Users());
    }

    /**
     * Проверка создания wsdl и существования всех методов сервиса
     *
     * @throws Exception
     */
    public function testWsdl()
    {
        $methods = get_class_methods(API::class);

        $strategy = new ArrayOfTypeSequence();
        $strategy->setDocumentationStrategy(new ReflectionDocumentation());

        $autodiscover = new AutoDiscover($strategy);
        $autodiscover
            ->setClass(API::class)
            ->setUri($this->post)
            ->setServiceName('SoapService');
        $wsdl = $autodiscover->generate();
        $wsdl = $wsdl->toXml();

        foreach($methods as $method) {
            if($method === '__construct' || $method === '__destruct') continue;
            $this->assertTrue((bool)strpos($wsdl, $method) !== false);
        }
    }

    public function testAddProcedureMspLocker()
    {
        $company = $this->loginCustomer()->getCompany();
        $this->getTestData()->createTestProcedureOpenList();
        $guid = self::PROCEDURE_GUID;
        $db = getDbInstance();
        $db->getProfiler()->setEnabled(true);
        $update = [
            'eis_registry_number' => new Zend_Db_Expr('null'),
            'registry_number' => new Zend_Db_Expr('null'),
            'oos_id' => new Zend_Db_Expr('null'),
            'actual' => new Zend_Db_Expr('false')
        ];

        $where = "oos_id = '$guid'";
        $db->update(DbTable_Procedures::NAME, $update, $where);
        $locker = new Model_Locker_SingletonInstanceMock();
        $message = null;
        try {
            $locker->lock($guid);
            //$decorator = new APIDecorator($api, $this->access);
            $procedureMsp = new CreateProcedureMsp;
            $procedureMsp->purchaseGuid = $guid;
            $procedureMsp->registryNumber = self::PROCEDURE_REGISTRY_NUMBER;

            $api = new API(
                $this->jwt,
                new Users(),
                new Procedure(),
                new Application(),
                new Mock(self::PROCEDURE_REGISTRY_NUMBER, 1, $company, $locker)
            );

            $api->createProcedureMsp($procedureMsp);

        } catch (Exception $e) {
            $message = $e->getMessage();
        } finally {
            $locker->unlock();
            $this->assertContains($locker::ERROR . $guid, $message ?? '');
        }
    }

    public function testAddProcedureMsp()
    {
        $company = $this->loginCustomer()->getCompany();
        $this->getTestData()->createTestProcedureOpenList();
        $procedureMsp = new CreateProcedureMsp;
        $procedureMsp->purchaseGuid = self::PROCEDURE_GUID;
        $procedureMsp->registryNumber = self::PROCEDURE_REGISTRY_NUMBER;

        $api = new API(
            $this->jwt,
            new Users(),
            new Procedure(),
            new Application(),
            new Mock(self::PROCEDURE_REGISTRY_NUMBER, 1, $company, null)
        );

        $result = $api->createProcedureMsp($procedureMsp);
        $this->assertTrue(!empty($result->procedureId) && is_numeric($result->procedureId));
    }

    public function testTender()
    {
        $customer = $this->login223Customer();
        $company = $customer->getCompany();
        $this->getTestData()->createTestProcedureOpenList();
        $procedureMsp = new CreateProcedureMsp;
        // purchaseGuid и registryNumber берём не из константы, т.к. там устаревшие данные
        $procedureMsp->purchaseGuid = '88ad6805-bdb1-a231-e539-33f780c5bc7f';
        $procedureMsp->registryNumber = '32000104369';

        $api = new API(
            $this->jwt,
            new Users(),
            new Procedure(),
            new Application(),
            new Mock($procedureMsp->registryNumber, 1, $company, null)
        );

        $result = $api->createProcedureMsp($procedureMsp);
        $this->assertTrue(!empty($result->procedureId) && is_numeric($result->procedureId));

        $procedure = \Model_Procedure::load($result->procedureId);

        $supplier1 = $this->loginSmbSupplier();
        $application1 = $this->createApplication($procedure);

        $supplier2 = $this->loginSmbSupplier();
        $application2 = $this->createApplication($procedure);

        $applications0 = $api->getApplications($procedureMsp);
        $this->assertApps($applications0, ['aEmpty' => true, 'fType' => 'ApplicationDoc']);

        $this->loginAdmin();
        $this->endRegistration($procedure, \Model_Lot::STATUS_FIRST_PARTS);
        \Core_Cache::clean(get_class($procedure).'R', $procedure->getId());

        $this->login($customer->getUsername(), $this->getTestData()->getDefaultPassword());
        $data = [];
        $this->createProtocol(\Model_Protocol::TYPE_REVIEW, $procedure, $data, true, ['count_accepted' => 2]);
        // заменяется гуид в процедуре, недосуг разбираться, поэтому просто возьмём новый гуид из процедуры
        $procedure = Model_Procedure::load($result->procedureId);
        $procedureMsp->purchaseGuid = $procedure->getOosId();

        $applications1 = $api->getApplications($procedureMsp);
        $this->assertApps($applications1, ['aEmpty' => false, 'fType' => 'ApplicationDoc']);

        $this->startCorrection($procedure);
        $this->login($supplier1->getUsername(), $this->getTestData()->getDefaultPassword());
        $correctedApplication1 = $this->createCorrectionApplication($procedure, $application1);
        $this->login($supplier2->getUsername(), $this->getTestData()->getDefaultPassword());
        $correctedApplication2 = $this->createCorrectionApplication($procedure, $application2);
        $this->endCorrection($procedure);

        $this->login($customer->getUsername(), $this->getTestData()->getDefaultPassword());
        //Протокол по оценке и сопоставлению
        $this->createProtocol(\Model_Protocol::TYPE_EVALUATION, $procedure, $data, true, []);

        $applications2 = $api->getApplications($procedureMsp);
        $this->assertApps($applications2, ['aEmpty' => false, 'fType' => 'ApplicationDoc']);

        //Протокол итогов
        $this->createProtocol(
            \Model_Protocol::TYPE_ITOG,
            $procedure,
            $data,
            true,
            ['winner_ids' => [$supplier1->getContragentId()]]
        );

        $applications3 = $api->getApplications($procedureMsp);
        $this->assertApps($applications3, ['aEmpty' => false, 'fType' => 'ApplicationDoc']);

        //Выбираем способ заключения контракта
        //Добавляем контракт
        //Размещаем контракт
        $contract = $this->createContract($procedure);

        //Согласуем и подписываем участником
        $this->login($supplier1->getUsername(), $this->getTestData()->getDefaultPassword());
        $this->signContract($contract, 'supplier');

        //Подписываем заказчиком
        $this->login($customer->getUsername(), $this->getTestData()->getDefaultPassword());
        $this->signContract($contract, 'customer');
    }

    public function testAuction () {

        $customer = $this->login223Customer();
        $company = $customer->getCompany();
        $this->getTestData()->createTestProcedureOpenList();
        $procedureMsp = new CreateProcedureMsp;
        // purchaseGuid и registryNumber берём не из константы, т.к. там устаревшие данные
        $procedureMsp->purchaseGuid = 'dbbb3088-9a74-3d7a-14d5-c7217c5b259a';
        $procedureMsp->registryNumber = '32000103349';

        $api = new API(
            $this->jwt,
            new Users(),
            new Procedure(),
            new Application(),
            new Mock($procedureMsp->registryNumber, 1, $company, null)
        );

        $result = $api->createProcedureMsp($procedureMsp);
        $this->assertTrue(!empty($result->procedureId) && is_numeric($result->procedureId));

        $procedure = Model_Procedure::load($result->procedureId);

        $supplier1 = $this->loginSmbSupplier();
        $application1 = $this->createApplication($procedure);

        $supplier2 = $this->loginSmbSupplier();
        $application2 = $this->createApplication($procedure);

        $applications0 = $api->getApplications($procedureMsp);
        $this->assertApps($applications0, ['aEmpty' => true, 'fType' => 'ApplicationDoc']);

        $this->loginAdmin();
        $this->endRegistration($procedure, Model_Lot::STATUS_FIRST_PARTS);
        \Core_Cache::clean(get_class($procedure).'R', $procedure->getId());

        $this->login($customer->getUsername(), $this->getTestData()->getDefaultPassword());

        $applications1 = $api->getApplications($procedureMsp);
        $this->assertApps($applications1, ['aEmpty' => false, 'fType' => 'ApplicationDoc']);

        $data = [];
        $this->createProtocol(\Model_Protocol::TYPE_REVIEW, $procedure, $data, true, ['count_accepted' => 2]);
        $procedure = Model_Procedure::load($result->procedureId);

        $this->startAuction($procedure);

        $this->login($supplier1->getUsername(), $this->getTestData()->getDefaultPassword());
        $this->createOffer($procedure, $application1, ['price' => $procedure->getLots()[0]->getStartPrice() - 10000]);
        $this->login($supplier2->getUsername(), $this->getTestData()->getDefaultPassword());
        $this->createOffer($procedure, $application2, ['price' => $procedure->getLots()[0]->getStartPrice() - 20000]);

        $this->loginAdmin();
        $this->closeAuction($procedure, Model_Lot::STATUS_EVALUATION);

        $applications2 = $api->getApplications($procedureMsp);
        $this->assertApps($applications2, ['aEmpty' => false, 'fType' => 'ApplicationDoc']);

        $this->login($customer->getUsername(), $this->getTestData()->getDefaultPassword());
        //Протокол по оценке и сопоставлению
        $this->createProtocol(\Model_Protocol::TYPE_EVALUATION, $procedure, $data, true, []);

        $applications3 = $api->getApplications($procedureMsp);
        $this->assertApps($applications3, ['aEmpty' => false, 'fType' => 'ApplicationDoc']);

        //Протокол итогов
        $this->createProtocol(
            \Model_Protocol::TYPE_ITOG,
            $procedure,
            $data,
            true,
            ['winner_ids' => [$supplier2->getContragentId()]]
        );

        $applications4 = $api->getApplications($procedureMsp);
        $this->assertApps($applications4, ['aEmpty' => false, 'fType' => 'ApplicationDoc']);

        //Выбираем способ заключения контракта
        //Добавляем контракт
        //Размещаем контракт
        $contract = $this->createContract($procedure);

        //Согласуем и подписываем участником
        $this->login($supplier2->getUsername(), $this->getTestData()->getDefaultPassword());
        $this->signContract($contract, 'supplier');

        //Подписываем заказчиком
        $this->login($customer->getUsername(), $this->getTestData()->getDefaultPassword());
        $this->signContract($contract, 'customer');
    }

    public function testGetFile()
    {
        $customer = $this->login223Customer();
        $company = $customer->getCompany();
        $this->getTestData()->createTestProcedureOpenList();
        $procedureMsp = new CreateProcedureMsp;
        // purchaseGuid и registryNumber берём не из константы, т.к. там устаревшие данные
        $procedureMsp->purchaseGuid = '88ad6805-bdb1-a231-e539-33f780c5bc7f';
        $procedureMsp->registryNumber = '32000104369';

        $api = new API(
            $this->jwt,
            new Users(),
            new Procedure(),
            new Application(),
            new Mock($procedureMsp->registryNumber, 1, $company, null)
        );

        $result = $api->createProcedureMsp($procedureMsp);
        $this->assertTrue(!empty($result->procedureId) && is_numeric($result->procedureId));

        $procedure = \Model_Procedure::load($result->procedureId);

        $supplier1 = $this->loginSmbSupplier();
        $application1 = $this->createApplication($procedure);

        $supplier2 = $this->loginSmbSupplier();
        $application2 = $this->createApplication($procedure);

        $this->loginAdmin();
        $this->endRegistration($procedure, \Model_Lot::STATUS_FIRST_PARTS);
        \Core_Cache::clean(get_class($procedure).'R', $procedure->getId());

        $this->login($customer->getUsername(), $this->getTestData()->getDefaultPassword());
        $data = [];
        $this->createProtocol(\Model_Protocol::TYPE_REVIEW, $procedure, $data, true, ['count_accepted' => 2]);
        // заменяется гуид в процедуре, недосуг разбираться, поэтому просто возьмём новый гуид из процедуры
        $procedure = Model_Procedure::load($result->procedureId);
        $procedureMsp->purchaseGuid = $procedure->getOosId();

        $applications1 = $api->getApplications($procedureMsp);
        $this->assertApps($applications1, ['aEmpty' => false, 'fType' => 'ApplicationDoc']);

        $files1 = [];
        foreach ($applications1 as $a) {
            foreach ($a->files as $f) {
                $fileTransfer = new FileTransfer;
                $fileTransfer->id = $f['id'];
                $fileTransfer->type = $f['type'];
                $file = $api->getFile($fileTransfer);
                $files1[] = $file;
            }
        }
        $this->assertFiles($files1);

        $this->startCorrection($procedure);
        $this->login($supplier1->getUsername(), $this->getTestData()->getDefaultPassword());
        $correctedApplication1 = $this->createCorrectionApplication($procedure, $application1);
        $this->login($supplier2->getUsername(), $this->getTestData()->getDefaultPassword());
        $correctedApplication2 = $this->createCorrectionApplication($procedure, $application2);
        $this->endCorrection($procedure);

        $this->login($customer->getUsername(), $this->getTestData()->getDefaultPassword());
        //Протокол по оценке и сопоставлению
        $this->createProtocol(\Model_Protocol::TYPE_EVALUATION, $procedure, $data, true, []);

        //Протокол итогов
        $this->createProtocol(
            \Model_Protocol::TYPE_ITOG,
            $procedure,
            $data,
            true,
            ['winner_ids' => [$supplier1->getContragentId()]]
        );

        $applications2 = $api->getApplications($procedureMsp);
        $this->assertApps($applications2, ['aEmpty' => false, 'fType' => 'ApplicationDoc']);

        $files2 = [];
        foreach ($applications2 as $a) {
            foreach ($a->files as $f) {
                $fileTransfer = new FileTransfer;
                $fileTransfer->id = $f['id'];
                $fileTransfer->type = $f['type'];
                $file = $api->getFile($fileTransfer);
                $files2[] = $file;
            }
        }
        $this->assertFiles($files2);

    }

}
