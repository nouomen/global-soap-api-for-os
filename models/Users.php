<?php

namespace SoapIntegration\Model;

use DbTable_Users;
use Exception;
use Zend_Db_Adapter_Exception;

class Users extends Model
{
    /**
     * @param string $login
     * @param string $password
     * @return int
     * @throws Exception
     */
    public function logIn(string $login, string $password): int
    {
        $password = md5($password);
        $select = $this->db
            ->select()
            ->from(DbTable_Users::NAME, ['id'])
            ->where('soap_integration_username=?', $login)
            ->where('soap_integration_password=?', $password)
            // todo выяснить нужно ли использовать это поле
            //->where('active=?', true)
        ;
        $result = $this->db->fetchOne($select);

        if (empty($result)) {
            throw new Exception('Ошибка авторизации', 401);
        }

        return $result;
    }

    /**
     * @param $value
     * @param string $filterField
     * @param string $returnField
     * @return string
     */
    public function getUser($value, string $filterField = 'id', string $returnField = 'id')
    {
        $select = $this->db
            ->select()
            ->from(DbTable_Users::NAME, [$returnField])
            ->where("$filterField=?", $value)
        ;
        return $this->db->fetchOne($select);
    }

    /**
     * Изменение логина и пароля
     *
     * @param int $userId
     * @param string $login
     * @param string $password
     * @return int
     * @throws Zend_Db_Adapter_Exception
     * @throws Exception
     */
    public function updateLoginData(int $userId, string $login, string $password): int
    {
        $existedId = $this->getUser($login, 'soap_integration_username');

        if (!empty($existedId) && $existedId !== $userId) {
            throw new Exception("Логин $login уже существует в ситсеме", 500);
        }
        $password = md5($password);
        return $this->db->update(DbTable_Users::NAME, [
            'soap_integration_username' => $login,
            'soap_integration_password' => $password,
        ], "id=$userId");
    }
}