<?php

namespace SoapIntegration\Model\Type\Response;

/**
 * Возвращает содержимое файла
 * Class FileData
 * @package SoapIntegration\Model\Type\Response
 */
class FileData
{
    /**
     * Cодержимое файла в формате base64
     * @var string
     */
    public $content = '';
}