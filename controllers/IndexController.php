<?php

namespace SoapIntegration;
// todo убрать на проде
ini_set('soapServices.wsdl_cache_enabled', 0);

use Core_Controller_Action;
use SoapIntegration\Model\Users;
use Zend\Soap\Wsdl\ComplexTypeStrategy\ArrayOfTypeSequence;
use SoapIntegration\Service\API;
use Zend\Soap\AutoDiscover;
use Zend\Soap\Server;
use Zend\Soap\Wsdl\DocumentationStrategy\ReflectionDocumentation;
use SoapIntegration\Model\Log;
use Zend_Exception;
use Exception;
use SoapIntegration\Service\Access;
use SoapIntegration\Service\JWT;
use SoapIntegration\Factory\AuthFactory;
use SoapIntegration\Service\APIDecorator;
use Zend_Db_Adapter_Exception;

class IndexController extends Core_Controller_Action
{
    private $post;
    private $wsdl;
    private $access;
    /** @var JWT */
    private $jwt;
    /** @var Log log */
    private $log;

    public function init()
    {
        $this->post = getConfigValue('general->site_url') . '/soap-integration/index/post';
        $this->wsdl = getConfigValue('general->site_url') . '/soap-integration/index/wsdl';
        $authFactory = new AuthFactory();
        $this->jwt = $authFactory();
        $this->log = new Log();
        $this->access = new Access($this->jwt, new Users());
    }

    /**
     * @remotable
     */
    public function wsdlAction()
    {
        header('Content-Type: text/xml');
        $strategy = new ArrayOfTypeSequence();
        $strategy->setDocumentationStrategy(new ReflectionDocumentation());

        $autodiscover = new AutoDiscover($strategy);
        $autodiscover
            ->setClass(API::class)
            ->setUri($this->post)
            ->setServiceName('SoapService');
        $wsdl = $autodiscover->generate();
        echo $wsdl->toXml();
        die;
    }

    /**
     * @throws Zend_Exception
     * @throws Zend_Db_Adapter_Exception
     * @throws Exception
     * @remotable
     */
    public function postAction()
    {
        // todo cache
        $api = getDiContainer()->get(API::class);
        $server = new Server();
        // в декораторе проверяем методы сервиса
        $decorator = new APIDecorator($api, $this->access);
        $server->setClass($decorator);
        $server->setReturnResponse(true);
        $server->setUri($this->wsdl);
        $response = $server->handle();
        $request = $server->getLastRequest();
        $userId = $decorator->getUserId();
        $procedureId = $decorator->getProcedureId();
        $action = $decorator->getMethod();
        $succeed = $decorator->getSucceed();
        $this->log->log($request, $response, $userId, $succeed, $action, $procedureId);
        echo $response;
        die;
    }

    /**
     * Получение логов интеграции
     *
     * @param array $params
     * @throws Zend_Exception
     * @throws Exception
     * @remotable
     */
    public function logAction(array $params)
    {
        $soapApiLog = new Log();
        $this->view->success = true;
        $this->view->result = $soapApiLog->loadLog($params);
    }

    /**
     * Получение soap логина для отображения в форме
     *
     * @throws Exception
     * @remotable
     */
    public function getLoginAction(array $params)
    {
        $user = new Users();
        $this->view->success = true;
        $this->view->result = $user->getUser(getActiveUser(), 'id', 'soap_integration_username');
    }

    /**
     * Изменение логина и пароля из формы
     *
     * @param array $params
     * @remotable
     */
    public function editLoginDataAction(array $params)
    {
        try {
            $userId = getActiveUser();
            $login = trim($params['soap_integration_username']);
            $pass = trim($params['soap_integration_password']);

            if (empty($userId) || empty($login) || empty($pass)) {
                throw new Exception('Невозможно сохранить данные. Форма заполнена неверно.');
            }

            $user = new Users();
            $user->updateLoginData($userId, $login, $pass);

            $this->view->success = true;
            $this->view->message = 'Данные сохранены.';
        } catch (Exception $e) {
            $this->view->success = false;
            $this->view->message = $e->getMessage();
        }
    }
}
