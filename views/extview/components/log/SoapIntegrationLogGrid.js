Ext.define('Application.components.SoapIntegrationLogGrid', {
  extend: 'Ext.grid.EditorGridPanel',
  frame: true,
  border: false,
  clicksToEdit: 2,
  initComponent: function () {
    this.requestData = [{
      tooltip: 'Request',
      icon: '/ico/soap-integration/up.png',
      text: '',
      scope: this,
      handler: function (grid, rowIndex) {
        this.popUpHandler(grid, rowIndex, 'request');
      }
    }];

    this.responseData = [{
      tooltip: 'Response',
      icon: '/ico/soap-integration/down.png',
      text: '',
      scope: this,
      handler: function (grid, rowIndex) {
        this.popUpHandler(grid, rowIndex, 'response');
      }
    }];

    Ext.apply(this, {
      store: this.getStore(),
      colModel: new Ext.grid.ColumnModel({
        defaults: {
          align: 'center',
          sortable: true,
          renderer: function (value, meta) {
            if (!!meta && !!value) {
              meta.attr = 'ext:qtip="' + value + '"';
            }
            return value;
          }
        },
        columns: this.getColumns()
      }),
      viewConfig: {
        forceFit: true,
        getRowClass: function (record, rowIndex, rp) {
          rp.tstyle += ' max-height: 55px; overflow: hidden; text-overflow: ellipsis;';
          if (rowIndex % 2 === 0) {
            return 'whitesmoke-bg';
          }
        }
      },
      bbar: renderPagingToolbar('Записи', this.getStore(), 25)
    });

    Application.components.SoapIntegrationLogGrid.superclass.initComponent.call(this);
  },
  getStore: function () {
    if (!this.store) {
      this.store = new Ext.data.DirectStore({
        directFn: RPC_soap_integration.Index.log,
        paramsAsHash: true,
        remoteSort: true,
        autoLoad: true,
        root: 'rows',
        idProperty: 'id',
        totalProperty: 'totalCount',
        fields: ['id', 'request', 'response', {
          name: 'date',
          type: 'date',
          dateFormat: 'c'
        }, 'procedure_id', 'procedure_registry_number', 'user_id', 'user_name', 'type', 'success', 'ip'],
        baseParams: {
          start: 0,
          limit: 25
        },
        sortInfo: {
          field: 'id',
          direction: 'DESC'
        }
      });
    }
    return this.store;
  },
  getColumns: function () {
    return [
      {header: 'ID', width: 10, dataIndex: 'id'},
      {header: 'Дата', width: 20, dataIndex: 'date', renderer: Ext.util.Format.dateRenderer('d.m.Y H:i:s')},
      {header: 'Номер процедуры', dataIndex: 'procedure_id', width: 15, hidden: true, renderer: this.procedureRenderer},
      {header: 'Тип', dataIndex: 'type', width: 25},
      {header: 'IP', dataIndex: 'ip', width: 15},
      {header: 'Статус', dataIndex: 'success', width: 10, renderer: this.successRenderer},
      {header: 'Процедура', dataIndex: 'procedure_registry_number', width: 15, renderer: this.procedureRenderer},
      {header: 'Номер пользователя', dataIndex: 'user_id', width: 15, hidden: true, renderer: this.userRenderer},
      {header: 'Пользователь', dataIndex: 'user_name', width: 15, renderer: this.userRenderer},
      {header: 'Запрос', xtype: 'textactioncolumn', width: 10, actionsSeparator: ' ', items: this.requestData},
      {header: 'Ответ', xtype: 'textactioncolumn', width: 10, actionsSeparator: ' ', items: this.responseData}
    ]
  },
  userRenderer: function (value, p, record) {
    if (isAdmin() && value) {
      return String.format('<a href="/#user/view/id/{0}" target="_blank" style = "text-decoration: none;">{1}</a>', record.get('user_id'), value);
    }
    return value || '';
  },
  procedureRenderer: function (value, p, record) {
    if (isAdmin() && record.get('procedure_id') && value) {
      return String.format('<a href="/#com/procedure/view/procedure/{0}" target="_blank" style = "text-decoration: none;">{1}</a>', record.get('procedure_id'), value);
    }
    return value || '';
  },
  successRenderer: function (value) {
    return value ? '<img src="/ico/soap-integration/success.png" ext:qtip="Успешно">' : '<img src="/ico/soap-integration/fail.png" ext:qtip="Ошибка">';
  },
  popUpHandler: function (grid, rowIndex, value)
  {
      var item = grid.store.getAt(rowIndex);
      if (!item || !item.data) {
        return;
      }
      var popUp = new Ext.Window({
        width: 500,
        height: 500,
        autoheight: true,
        layout: 'fit',
        closeAction: 'hide',
        title: 'XML',
        modal: true,
        items: [{
          xtype: 'textarea',
          border: false,
          readOnly: true,
          value: item.data[value]
        }]
      });
      popUp.show();
  }
});
