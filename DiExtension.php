<?php

namespace SoapIntegration;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\XmlFileLoader;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\Config\Loader\DelegatingLoader;
use Symfony\Component\Config\Loader\LoaderResolver;

/**
 * Экстеншнт DI, грузящий настройки из configs/services.xml данного модуля
 */
class DiExtension extends Extension
{

  public function load(array $config, ContainerBuilder $container)
  {
    $path = __DIR__ . '/config';
    $fileLocator = new FileLocator($path);
    $loaderResolver = new LoaderResolver([
        new YamlFileLoader($container, $fileLocator),
        new XmlFileLoader($container, $fileLocator),
    ]);
    $loader = new DelegatingLoader($loaderResolver);
    $loader->load('services.yaml');
  }

  /**
   * @return string
   */
  public function getAlias()
  {
    return 'soapintegration';
  }
}
