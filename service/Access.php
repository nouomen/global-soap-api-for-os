<?php
/**
 * Работа с токеном и правами
 */
namespace SoapIntegration\Service;

use SoapIntegration\Model\Users;
use Exception;

/**
 * Класс отвечает за обработку токена
 *
 * Access
 */
class Access
{
    private $token;
    /** @var JWT */
    private $jwt;
    /** @var Users */
    private $users;

    public function __construct(JWT $jwt, Users $users)
    {
        // AUTHORIZATION with bearer
        $this->setToken(empty($_SERVER['HTTP_AUTHORIZATION']) ? null : $this->getTokenInString($_SERVER['HTTP_AUTHORIZATION']));
        $this->jwt = $jwt;
        $this->users = $users;

    }

    public function getUserId($token)
    {
       return $this->jwt->getUserId($token);
    }

    /**
     * Проверка токенов
     *
     * @return int
     * @throws Exception
     */
    public function hasAccess(): int
    {
        $token = $this->getToken();

        if (empty($token)) {
            throw new Exception('Токен не найден или не валиден', 401);
        }

        $userIdFromToken = $this->getUserId($token);

        if (empty($userIdFromToken)) {
            throw new Exception('Авторизационный токен недействителен (0)', 401);
        }

        $result = $this->users->getUser($userIdFromToken);

        if (empty($result)) {
            throw new Exception("Юзер с $userIdFromToken не найден", 401);
        }

        return $result;
    }

    /**
     * Парсинг токена из заголовков. Пример: bearer 10d1db0098a7bc29ca9cc4cef50cc93d
     *
     * @param string $str
     * @return string
     */
    public function getTokenInString(string $str): string
    {
        $arr = explode(' ', trim($str));
        $token = $arr[1] ?? '';
        return trim($token);
    }

    /**
     * Устанавливает токен.
     * Используется только в unit-тестах.
     *
     * @param $token string
     */
    public function setToken($token)
    {
        $this->token = $token;
    }

    public function getToken()
    {
      return $this->token;
    }
}
