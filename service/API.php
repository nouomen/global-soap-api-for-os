<?php

namespace SoapIntegration\Service;

use Exception;
use Model_Procedure;
use SoapIntegration\Model\Type\Response\CreateProcedureMspResult;
use SoapIntegration\Model\Type\Response\TokenData;
use SoapIntegration\Model\Users as UsersModel;
use SoapIntegration\Model\Procedure as ProcedureModel;
use SoapIntegration\Service\OOSProcedures\IStrategy;
use SoapIntegration\Model\Type\Response\Application;
use SoapIntegration\Model\Application as ApplicationModel;
use SoapIntegration\Model\Type\Response\FileData;
use SoapIntegration\Model\File;

class API
{
    use ServiceTrait;

    const DEFAULT_ERROR_CODE = 500;
    private $jwt;
    private $userModel;
    private $procedureModel;
    private $oosProcedures;
    private $applicationModel;
    private $locker;

    /**
     * API constructor.
     * @param JWT $jwt
     * @param UsersModel $user
     * @param ProcedureModel $procedure
     * @param ApplicationModel $application
     * @param IStrategy $oosProcedures
     */
    public function __construct(
        JWT $jwt,
        UsersModel $user,
        ProcedureModel $procedure,
        ApplicationModel $application,
        IStrategy $oosProcedures
    )
    {
        $this->userModel = $user;
        $this->oosProcedures = $oosProcedures;
        $this->jwt = $jwt;
        $this->procedureModel = $procedure;
        $this->oosProcedures = $oosProcedures;
        $this->applicationModel = $application;
    }

    /**
     * Получение токена и информации по пользователю
     *
     * @param \SoapIntegration\Model\Type\Authorization $authorization
     * @return \SoapIntegration\Model\Type\Response\TokenData
     * @throws Exception
     */
    public function getToken($authorization)
    {
        $userId = $this->userModel->logIn($authorization->login, $authorization->password);
        $tokenData = new TokenData;
        $tokenData->token = $this->jwt->getAuthorizationToken($userId);
        $tokenData->expired = $this->jwt->getTtl();
        return $tokenData;
    }

    /**
     * Добавление мсп процедуры
     *
     * @param \SoapIntegration\Model\Type\CreateProcedureMsp $procedureMsp
     * @return \SoapIntegration\Model\Type\Response\CreateProcedureMspResult
     * @throws Exception
     */
    public function createProcedureMsp($procedureMsp)
    {
        if (!$this->isGuid($procedureMsp->purchaseGuid)) {
            throw new Exception($procedureMsp->purchaseGuid . ' не является валидным guid', self::DEFAULT_ERROR_CODE);
        }

        $procedureMspResult = new CreateProcedureMspResult;

        // возможно процедура уже добавлена кроном, тогда тянуть процедуру не нужно
        $id = $this->procedureModel->getProcedureId($procedureMsp->purchaseGuid);

        if (empty($id)) {
            $result = $this->oosProcedures->getProcedureFromOOS($procedureMsp->purchaseGuid);
            $errors = $this->getOosErrorsInString($result['errors'] ?? [], $result['warning'] ?? []);

            if (!empty($errors)) {
                throw new Exception('[Ошибка из ЕИС. Procedure import:] ' . $errors, self::DEFAULT_ERROR_CODE);
            }

            $id = $this->procedureModel->getProcedureId($procedureMsp->purchaseGuid);
        }

        if (empty($id)) {
            throw new Exception("$procedureMsp->purchaseGuid Процедруа не была добавлена", self::DEFAULT_ERROR_CODE);
        }

        $this->procedureModel->setApiNotch($id);
        $procedureMspResult->procedureId = $id;
        return $procedureMspResult;

    }

    /**
     * Изменение мсп процедуры
     *
     * @param \SoapIntegration\Model\Type\UpdateProcedureMsp $procedureMsp
     * @return string
     * @throws Exception
     */
    public function updateProcedureMsp($procedureMsp)
    {
        if (!$this->isGuid($procedureMsp->purchaseGuid)) {
            throw new Exception($procedureMsp->purchaseGuid . ' не является валидным guid', self::DEFAULT_ERROR_CODE);
        }
        // возможно хватит простого ответа в виде строки
        //$procedureMspResult = new UpdateProcedureMspResult;
        $existingProcedureVersion = $this->procedureModel->getProcedureVersion($procedureMsp->id);
        // придется обновить процедуру
        if ($procedureMsp->version > $existingProcedureVersion) {

            $result = $this->oosProcedures->getProcedureFromOOS($procedureMsp->purchaseGuid);

            $errors = $this->getOosErrorsInString($result['errors'] ?? [], $result['warning'] ?? []);
            if (!empty($errors)) {
                throw new Exception('[From OOSProcedures] ' . $errors, self::DEFAULT_ERROR_CODE);
            }

            $isProcedureAdded = (bool)$this->procedureModel->getProcedureId(
                $procedureMsp->purchaseGuid,
                $procedureMsp->version
            );

            if (!$isProcedureAdded) {
                throw new Exception("Процедруа $procedureMsp->id не была обновена", self::DEFAULT_ERROR_CODE);
            }

            $result = "Процедура $procedureMsp->id успешно обновлена";
        } elseif($procedureMsp->version < $existingProcedureVersion) {
            $result = "Версия ЭТП $existingProcedureVersion  выше передаваемой $procedureMsp->version";
        } else {
            $result = "Процедура $procedureMsp->id находится в актуальном состоянии";
        }

        return $result;
    }

    /**
     * Получение заявок процедуры на разных этапах
     *
     * @param \SoapIntegration\Model\Type\Applications $procedure
     * @return \SoapIntegration\Model\Type\Response\Application[]
     * @throws Exception
     */
    public function getApplications($procedure)
    {
        if (empty($procedure->procedureId)) {
            if (!empty($procedure->purchaseGuid)) {
                if (!$this->isGuid($procedure->purchaseGuid)) {
                    throw new Exception($procedure->purchaseGuid . ' не является валидным guid', self::DEFAULT_ERROR_CODE);
                }

                $procedureId = $this->procedureModel->getProcedureId($procedure->purchaseGuid);

            } elseif (!empty($procedure->registryNumber)) {
                $procedureId = $this->procedureModel->getProcedureIdByRegistryNumber($procedure->registryNumber);
            } elseif (!empty($procedure->eisRegistryNumber)) {
                $procedureId = $this->procedureModel->getProcedureIdByRegistryNumber($procedure->eisRegistryNumber, true);
            } else {
                $procedureId = null;
            }
        } else {
            $procedureId = $procedure->procedureId;
        }

        if(empty($procedureId)) {
            throw new Exception('Процедура не найдена', self::DEFAULT_ERROR_CODE);
        }

        /** @var Model_Procedure $procedure */
        $procedure = $this->procedureModel->getProcedureObject($procedureId);

        if( $this->procedureModel->isMsp($procedure) === false) {
            throw new Exception('Процедура не является МСП', self::DEFAULT_ERROR_CODE);
        }

        $lots = $this->procedureModel->getLots($procedure);

        $allApplications = [];

        foreach($lots as $lot) {

            if($lot['status'] === $this->procedureModel::STATUS_PUBLISHED) {
                continue;
            }

            $applications = $this->applicationModel->getApplications($lot['id']);
            // todo обсудить аук
            foreach($applications as $application){
                $files = $this->applicationModel->getDocsByApplication($application['app_id']);
                $offerOrAdditionalPrice = $this->applicationModel->getOfferOrAdditionalPrice($application['app_id']);
                $applicationObject = new Application;
                $applicationObject->eisRegistryNumber = $this->procedureModel->getEisRegistryNumber($procedure);
                $applicationObject->registryNumber = $this->procedureModel->getRegistryNumber($procedure);
                $applicationObject->lotNumber = $lot['number'];
                $applicationObject->date = $application['date'];
                $applicationObject->price = $application['price'] ?? null;
                $applicationObject->priceOffer = $offerOrAdditionalPrice['priceOffer'];
                $applicationObject->offerDiscount = $offerOrAdditionalPrice['offerDiscount'];
                $applicationObject->additionalPrice = $offerOrAdditionalPrice['additionalPrice'];
                $applicationObject->additionalDiscount = $offerOrAdditionalPrice['additionalDiscount'];
                $applicationObject->nds = isset($application['price']) ? $this->applicationModel->getNdsCode($application['nds_id']) : null;
                $applicationObject->type = $application['aptype'];
                $applicationObject->orderNumber = $application['order_number_added'];
                $applicationObject->inn = $application['supplier_inn'] ?? null;
                $applicationObject->kpp = $application['supplier_kpp'] ?? null;
                $applicationObject->ogrn = $application['supplier_ogrn'] ?? null;
                $applicationObject->address = $application['address'] ?? null;
                $applicationObject->organisationShortName = $application['full_name'] ?? null;
                $applicationObject->organisationFullName = $application['short_name'] ?? null;
                $applicationObject->contactPersonName = $application['contact_fio'] ?? null;
                $applicationObject->contactPersonEmail = $application['supplier_email'] ?? null;
                $applicationObject->contactPersonPhone = $application['contact_phone'] ?? null;
                $applicationObject->currencyCode = isset($application['price']) ? $this->procedureModel->getCurrencyCode($procedure) : null;
                $applicationObject->files = $files;
                $allApplications[] = $applicationObject;
            }
        }

        return $allApplications;
    }

    /**
     * Получение содержимого файла по заявке
     *
     * @param \SoapIntegration\Model\Type\FileTransfer $fileTransfer
     * @return \SoapIntegration\Model\Type\Response\FileData
     * @throws Exception
     */
    public function getFile($fileTransfer)
    {
        if (!$fileTransfer->type || !$fileTransfer->id) {
            throw new Exception('Не передан идентификатор или тип файла. Получение содержимого файла невозможно.', self::DEFAULT_ERROR_CODE);
        }

        $file = new FileData;

        $file->content = File::getFileData($fileTransfer);

        return $file;
    }
}