Ext.onReady(function () {
  Ext.override(Application.controllers.defaultModule.UserController, {
    changepasswordAction : function (params, app, panel) {
      panel.add({
        xtype: 'Application.components.actionPanel',
        cmpType: 'Application.components.LoginEditForm',
        title: 'Авторизационные данные для SOAP интерграции',
        cmpParams: {
          company_id: params.contragent||Main.contragent.id
        }
      });
    }
  });
});