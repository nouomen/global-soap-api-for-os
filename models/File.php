<?php

namespace SoapIntegration\Model;

use ResponseException;
use SoapIntegration\Model\Type\FileTransfer;

/**
 * Class File
 * @package SoapIntegration\Model
 */
class File extends Model
{

    /**
     * Метод возвращает название существующего класса
     * @param FileTransfer $fileTransfer
     * @return mixed
     */
    public static function getClassName(FileTransfer $fileTransfer)
    {
        $class = $fileTransfer->type;

        $approvedModels = array(
            'SupplierFiles', 'CustomerFiles', 'LotDocuments', 'ProtocolFiles',
            'ApplicationDoc', 'ProcedureClaimDoc', 'RequestApplicDocs',
            'DocExplainFiles', 'Rosneft_SupplierAccreditationFiles', 'UserFiles',
            'Rosneft_SupplierPrequalificationFiles', 'RequestDocs', 'DiscussionFunctional',
            'Rosneft_PrequalificationLotRequirementDocs', 'RequestDecisionDocs',
            'ResultDocs', 'Rosneft_PrequalificationApplicationFile', 'ContragentRegulatoryDocs',
            'Contract_File', 'SecurityDepositLotSupplierFiles', 'ProtocolAppsFiles', 'EruzContractorFiles'
        );

        if (!empty($class) && !in_array($class, $approvedModels)) {
            throw new ResponseException('Класс модели не передан или отсутствует в списке.', 404, null, $class);
        }

        $className = 'Model_' . $class;

        if (!class_exists($className) || !method_exists($className, 'load')) {
            throw new ResponseException('Класс модели не существует или не поддерживает метод загрузки файла.', 405);
        }

        return $className;
    }

    /**
     * Получение содержимого файла заявки
     * @param FileTransfer $fileTransfer
     * @return string
     * @throws \ResponseException
     */
    public static function getFileData(FileTransfer $fileTransfer) {
        $result = '';
        $model = self::getClassName($fileTransfer);
        $doc = $model::load($fileTransfer->id);

        if (!$doc) {
            throw new ResponseException('Файл с идентификатором ' . $fileTransfer->id . ' не существует.', 404);
        }

        if (!file_exists($doc->getFilePath())) {
            throw new ResponseException('Файл не найден.', 404, null, ['file_path' => $doc->getFilePath()]);
        }

        if ($doc->checkHash() && $doc->checkAccessAllowed(false)) {
            $data = $doc->getFileContents();
            $result = base64_encode($data);
        }

        return $result;
    }
}