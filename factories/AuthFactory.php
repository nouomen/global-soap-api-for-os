<?php
namespace SoapIntegration\Factory;

use SoapIntegration\Service\JWT;

class AuthFactory
{
    public function __invoke()
    {
        $ttl = (int)getConfigValue('soap_integration->ttl', 3600);
        $key =  getConfigValue('soap_integration->jwt_key', 'key');

        return new JWT($key, $ttl);
    }
}
