<?php
namespace SoapIntegration\Factory;

use SoapIntegration\Service\OOSProcedures\Download;

class OOSProceduresFactory
{
    public function __invoke()
    {
        return new Download();
    }
}
