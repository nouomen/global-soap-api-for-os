<?php

namespace SoapIntegration\Service\OOSProcedures;

interface IStrategy
{
    public function getProcedureFromOOS(string $guid);
}

