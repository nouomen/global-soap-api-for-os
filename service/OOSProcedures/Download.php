<?php

namespace SoapIntegration\Service\OOSProcedures;

use SoapIntegration\Service\OOSProcedures\IStrategy;
use Model_OosExchange;
use Exception;

class Download implements IStrategy
{
    /**
     * @param string $guid
     * @return array
     * @throws Exception
     */
    public function getProcedureFromOOS(string $guid)
    {
        return Model_OosExchange::downloadWebserviceRespByGuid($guid);
    }

}