<?php
/**
 * Любое взаимеодействие с моделями cometp должно проходить через этот класс. Даже если модели cometp разнятся
 * от площадки к площадке, все исправления будут проходить только в этом классе
 */

namespace SoapIntegration\Model;

use DbTable_Procedures;
use Exception;
use Model_Lot;
use Zend_Db_Adapter_Exception;
use Model_Procedure;

class Procedure extends Model
{
    const STATUS_PUBLISHED = Model_Lot::STATUS_PUBLISHED;

    /**
     * Получение id процедуры
     *
     * @param string $guid Гуид процедуры
     * @param int $version Версия процедуры
     * @return int Id процедуры
     * @throws Exception
     */
    public function getProcedureId(string $guid, int $version = 0): int
    {
        $select = $this->db
            ->select()
            ->from(DbTable_Procedures::NAME, ['id'])
            ->where('oos_id=?', $guid);

        if ($version) {
            $select->where('version=?', $version);
        }

        return $this->db->fetchOne($select);
    }

    /**
     * @param string $registryNumber Реестровый номер
     * @param bool $isEIS это Реестровый номер ЕИСа или ЭТП
     * @return int
     */
    public function getProcedureIdByRegistryNumber(string $registryNumber, bool $isEIS = false): int
    {
        $fieldName = $isEIS ? 'eis_registry_number' : 'registry_number';
        $select = $this->db
            ->select()
            ->from(DbTable_Procedures::NAME, ['id'])
            ->where("$fieldName=?", $registryNumber);

        return $this->db->fetchOne($select);
    }

    public function getProcedureVersion(int $id): int
    {
        $select = $this->db
            ->select()
            ->from(DbTable_Procedures::NAME, ['version'])
            ->where('id=?', $id);
        return $this->db->fetchOne($select);
    }

    /**
     * Является ли процедура мсп
     *
     * @param Model_Procedure $procedure
     * @return bool
     */
    public function isMsp(Model_Procedure $procedure): bool
    {
        return (bool)$procedure->getSmallBizOnly();
    }

    public function getProcedureObject(int $id): Model_Procedure
    {
        return Model_Procedure::load($id);
    }

    public function getCurrencyCode(Model_Procedure $procedure)
    {
        return $procedure->getCurrencyObject()->getDigitalCode();
    }

    public function getEisRegistryNumber(Model_Procedure $procedure): string
    {
        return $procedure->getEisRegistryNumber();
    }

    public function getRegistryNumber(Model_Procedure $procedure): string
    {
        return $procedure->getRegistryNumber();
    }

    public function getLots(Model_Procedure $procedure)
    {
        $lots = [];
        foreach($procedure->getLots() as $key => $lot) {
            $lots[$key]['id'] = $lot->getId();
            $lots[$key]['number'] = $lot->getNumber();
            $lots[$key]['status'] = $lot->getStatus();
        }

        return $lots;
    }


    /**
     * Поставить метку, что процедура пришла из API
     *
     * @param int $id
     * @return int
     * @throws Zend_Db_Adapter_Exception
     */
    public function setApiNotch(int $id): int
    {
        return $this->db->update(DbTable_Procedures::NAME, ['is_from_api' => true], "id=$id");
    }
}
