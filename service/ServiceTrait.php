<?php
namespace SoapIntegration\Service;

use Exception;

trait ServiceTrait
{
    /**
     * Возвратить все ошибки и предупреждения с оос в виде строки
     *
     * @param array $errors
     * @param array $warnings
     * @return string
     * @throws Exception
     */
    private function getOosErrorsInString(array $errors, array $warnings): string
    {
        $issues = [];

        foreach ($errors as $error) {
            $issues[] = \is_string($error) ? $error : $error->getMessage();
        }

        foreach ($warnings as $warning) {
            $issues[] =  \is_string($warning) ? $warning : $warning->getMessage();
        }

        return empty($issues) ? '' : implode(', ', $issues);
    }

    /**
     * Является ли строка гуидом
     *
     * @param string $str
     * @return bool
     */
    private function isGuid(string $str): bool
    {
        return preg_match("/^(\{)?[a-f\d]{8}(-[a-f\d]{4}){4}[a-f\d]{8}(?(1)\})$/i", $str);
    }
}