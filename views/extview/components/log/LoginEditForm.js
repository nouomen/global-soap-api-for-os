Application.components.LoginEditForm = Ext.extend(Ext.form.FormPanel, {
  frame: true,
  labelWidth: 250,
  initComponent: function () {
    var component = this, fieldset_cmp = Ext.id();

    Ext.apply(this, {
      bodyCssClass: 'subpanel',
      width: 500,
      items: [
        {
          xtype: 'fieldset',
          title: 'Введите новые данные',
          layout: 'form',
          id: fieldset_cmp,
          style: 'margin: 0px',
          labelWidth: 120,
          defaults: {
            xtype: 'textfield',
            allowBlank: false,
            anchor: '100%'
          },
          items: [
            {
              xtype: 'hidden',
              name: 'user_id',
              value: Main.user.id
            },
            {
              name: 'soap_integration_username',
              fieldLabel: 'Логин' + REQUIRED_FIELD,
              listeners: {
                render: function (c) {
                  performRPCCall(
                    RPC_soap_integration.Index.getLogin,
                    [{user_id: Main.user.id}],
                    {},
                    function (response) {
                      this.setValue(response);
                    }.bind(this)
                  );
                }
              }
            },
            {
              fieldLabel: 'Пароль' + REQUIRED_FIELD,
              name: 'soap_integration_password',
              inputType: 'password'
            },
            {
              fieldLabel: 'Повторить пароль' + REQUIRED_FIELD,
              name: 'soap_integration_password_repeated',
              inputType: 'password'
            }
          ]
        }
      ],
      buttonAlign: 'right',
      buttons: [
        {
          text: 'Сохранить',
          handler: function () {
            var values = {};
            collectComponentValues(component, values, false);
            if(!values.soap_integration_password || !values.soap_integration_password_repeated || !values.soap_integration_username) {
              Ext.Msg.alert('Ошибка', 'Заполните все поля!');
              return false;
            }

            if (values.soap_integration_password !== values.soap_integration_password_repeated) {
              Ext.Msg.alert('Ошибка', 'Пароли не совпадают!');
              return false;
            }

            performRPCCall(RPC_soap_integration.Index.editLoginData, [values], null, function (resp) {
              echoResponseMessage(resp);
            });

          }
        }
      ]
    });
    Application.components.OosAuthEditForm.superclass.initComponent.call(this);
  }
});