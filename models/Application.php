<?php

namespace SoapIntegration\Model;

use Model_Application;
use Exception;
use Model_Lot;
use Model_ApplicationDoc;
use Model_VocabNds;
use Model_Procedure;

class Application extends Model
{
    /**
     * @param int $lotId
     * @throws Exception
     * @return array
     * @throws Exception
     */
    public function getApplications(int $lotId): array
    {
        $lot = Model_Lot::load($lotId);
        $type = $lot->getStatus() >= Model_Lot::STATUS_SECOND_PARTS ? 2 : 1;
        return Model_Application::getLotApps($lot, $type);
    }

    public function getNdsCode(int $id)
    {
        return Model_VocabNds::load($id)->getCode();
    }

    /**
     * @param int $applicationId
     * @throws Exception
     * @return array
     * @throws Exception
     */
    public function getDocsByApplication(int $applicationId): array
    {
        $docs = Model_ApplicationDoc::loadBy('application_id', $applicationId);
        $listDocs = [];
        foreach ($docs as $doc) {
            if (!$doc->checkAccessAllowed(false)) {
                continue;
            }

            $d['id'] = $doc->getId();
            $d['type'] = str_replace('Model_', '', get_class($doc));
            if ($doc instanceof \Model_ConfidentialityProviderInterface && !$doc->isOwnerConfidential()) {
                $signature = $doc->getSignatureString();
                $d['signature'] = base64_encode($signature);
            }
            $listDocs[] = $d;
        }
        return $listDocs;
    }

    /**
     * @param int $applicationId
     * @throws Exception
     * @return array
     * @throws Exception
     */
    public function getOfferOrAdditionalPrice(int $applicationId): array
    {
        $application = Model_Application::load($applicationId);
        $lot = Model_Lot::load($application->getLotId());
        $prices = [
            'priceOffer' => null,
            'offerDiscount' => null,
            'additionalPrice' => null,
            'additionalDiscount' => null
        ];

        if ($lot->calculateLotShowStage() === 3) {
            $procedure = Model_Procedure::load($lot->getProcedureId());
            $procedureType = $procedure->getProcedureType();
            $isSMB = $procedure->getSmallBizOnly();
            switch ($procedureType) {
                case Model_Procedure::PROCEDURE_TYPE_AUC_DESC:
                    $lastOfferPrice = \Model_Offers::loadLastOfferPriceBySupplier($lot, $application->getSupplierId());
                    if ($lot->getApplicPriceAsDiscount()) {
                        $prices['offerDiscount'] = $lastOfferPrice;
                    } else {
                        $prices['priceOffer'] = $lastOfferPrice;
                    }
                    break;
                case Model_Procedure::PROCEDURE_TYPE_TENDER:
                    if ($isSMB && $procedure->getWithApplicCorrection() && $application->getCorrection()) {
                        if ($lot->getApplicPriceAsDiscount()) {
                            $prices['additionalDiscount'] = $application->getDiscount();
                        } else {
                            $prices['additionalPrice'] = $application->getPrice();
                        }
                    }
                    break;
            }
        }

        return $prices;
    }

}
