<?php

namespace SoapIntegration\Model\Type\Response;

/**
 * Общая часть заявки
 * @package SoapIntegration\Model\Type\Response
 */
class Application
{
    /**
     * Реестровому номер процедуры ЕИСа
     * @var string
     */
    public $eisRegistryNumber = '';

    /**
     * Реестровый номер процедуры ЭТП
     * @var string
     */
    public $registryNumber = '';

    /**
     * Номер лота
     * @var int
     */
    public $lotNumber = 0;

    /**
     * Дата и время подачи предложения с часовым поясом
     * @var string
     */
    public $date = '';

    /**
     * Порядковый номер заявки
     * @var int
     */
    public $orderNumber = 0;

    /**
     * Тип предложения (основное или дополнительное)
     * @var string
     */
    public $type = '';

    /**
     * Цена заявки
     * @var float
     */
    public $price;

    /**
     * Ндс
     * @var int
     */
    public $nds;

    /**
     * Процент скидки (с символом %)
     * @var string
     */
    public $discount;

    /**
     * Код валюты
     * @var int
     */
    public $currencyCode;

    /**
     * Полное наименование организации
     * @var string
     */
    public $organisationFullName;

    /**
     * Краткое наименование организации
     * @var string
     */
    public $organisationShortName;

    /**
     * Инн
     * @var string
     */
    public $inn;

    /**
     * Кпп
     * @var string
     */
    public $kpp;

    /**
     * Огрн
     * @var string
     */
    public $ogrn;

    /**
     * Почтовый адрес
     * @var string
     */
    public $address;

    /**
     * ФИО контактного лица
     * @var string
     */
    public $contactPersonName;

    /**
     * Email контактного лица
     * @var string
     */
    public $contactPersonEmail;

    /**
     * Телефон контактного лица
     * @var string
     */
    public $contactPersonPhone;

    /**
     * Cписок файлов
     * @var array [
     *   module => модуль, string
     *   type => тип файла (класс), string
     *   id => идентификатор файла, int
     *   signature => подпись файла в кодировке base64, string
     * ]
     */
    public $files;

    /**
     * Последняя цена предложения участника на торгах
     * @var array
     */
    public $priceOffer;

    /**
     * Цена дополнительного предложения участника на этапе "Дополнительная подача предложений" (конкурс МСП)
     * @var array
     */
    public $additionalPrice;

    /**
     * Процент скидки предложения участника на торгах
     * @var array
     */
    public $offerDiscount;

    /**
     * Процент скидки дополнительного предложения участника на этапе "Дополнительная подача предложений" (конкурс МСП)
     * @var array
     */
    public $additionalDiscount;
}