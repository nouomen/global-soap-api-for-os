<?php
namespace SoapIntegration\Model;

use SoapIntegration\Model\DbTable\Log as DbTableLog;
use Zend_Db_Adapter_Exception;
use Zend_Db_Select;
use Zend_Db_Expr;
use Zend_Exception;

class Log extends Model
{
    /**
     * Загрузка логов
     *
     * @param $params
     * @return array | bool
     * @throws Zend_Exception
     */
    public function loadLog($params)
    {
        $defaults = ['limit' => 25, 'start' => 0, 'dir' => 'ASC', 'sort' => 'id'];
        $params = array_merge($defaults, $params);
        /** @var Zend_Db_Select $select */
        $select = $this->db->select()
            ->from(['api' => 'soap_api_log'], ['id', 'date', 'request', 'response', 'user_id', 'procedure_id', 'type', 'success', 'ip'])
            ->joinLeft(['u' => 'users'], 'u.id=api.user_id', [])
            ->joinLeft(['p' => 'procedures'], 'p.id=api.procedure_id', ['procedure_registry_number' => 'registry_number', 'organizer_contragent_id', 'procedure_type'])
            ->columns(['user_name' => new Zend_Db_Expr('u.first_name || \' \' || u.last_name')]);

        $sortParams = createSortLimitFromPost($params);
        if (!empty($sortParams['order'])) {
            $select->order($sortParams['order']);
        }

        list($rows, $count) = getPagerData($select, $params['start'], $params['limit'], false);
        return ['totalCount' => $count, 'rows' => $rows];
    }

    /**
     * Логирование
     *
     * @param string $request
     * @param string $response
     * @param int $userId
     * @param string $type
     * @param bool $success
     * @param $procedureId
     * @return int
     * @throws Zend_Db_Adapter_Exception
     */
    public function log(string $request, string $response, $userId, bool $success, string $type, $procedureId): int
    {
        return $this->db->insert(DbTableLog::NAME, [
            'request' => $request,
            'response' => $response,
            'user_id' => $userId,
            'success' => $success ? 'true' : 'false',
            'type' => $type,
            'procedure_id' => $procedureId,
            'date' => date('c'),
            'ip' =>  getActiveIpAddress()
        ]);
    }
}