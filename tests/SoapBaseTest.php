<?php

namespace SoapIntegration\Tests;

use EventRegistrator\Tests\EventFactory\Fz223\BaseTest;

class SoapBaseTest extends BaseTest
{
    /**
     * Проверяет наличие заявок в массиве и соответствие условиям
     * @param $apps array - массив заявок
     * @param $condition array - условия для проверки:
     *   aEmpty - массив заявок пустой, bool
     *   fType - тип класса файла заявки, string
     */
    public function assertApps($apps, $conditions) {
        if ($conditions['aEmpty']) {
            $this->assertSame( 0, count($apps), 'Массив заявок не пустой.');
        } else {
            $this->assertTrue(count($apps) > 0, 'Массив заявок пустой.');
        }
        foreach ($apps as $app) {
            $this->assertTrue(count($app->files) > 0, 'Массив файлов заявок пустой.');
            foreach ($app->files as $f) {
                $this->assertNotEmpty($f['id'], 'Идентификатор файла отсутствует.');
                $this->assertNotEmpty($f['type'], 'Тип класса не определён.');
                $this->assertEquals($conditions['fType'], $f['type'], 'Неожиданный тип класса для файла заявки.');
            }
        }
    }

    /**
     * Проверяет наличие файлов и их содержимое
     * @param $files array
     */
    public function assertFiles($files) {
        $this->assertTrue(count($files) > 0, 'Массив файлов пустой.');
        foreach ($files as $f) {
            $this->assertNotEmpty($f->content, 'Содержимое файла отсутствует.');
            $this->assertTrue(!!base64_decode($f->content, true) === true, 'Файл передан не в формате base64.');
        }
    }
}